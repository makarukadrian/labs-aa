package pk.labs.LabA;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "impl.DisplayBean";
    public static String controlPanelImplClassName = "impl.ControlpanelBean";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.SterowanieWinda";
    public static String mainComponentImplClassName = "impl.MainBean";
    // endregion

    // region P2
    public static String mainComponentBeanName = "modul";
    public static String mainFrameBeanName = "frame";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "silly";
    // endregion
}
